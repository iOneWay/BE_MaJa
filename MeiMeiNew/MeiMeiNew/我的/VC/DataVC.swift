//
//  DataVC.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class DataVC: BaseViewController {

    let nameLib: UILabel = {
        let temp = UILabel()
        temp.text = "昵称"
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level14)
        return temp
    }()
    
    let textInput: UITextField = {
        let temp = UITextField()
        temp.backgroundColor = UIColor.white
        temp.font = FontAsset.PingFangSC_Regular.size(.Level14)
        temp.placeholder = "  输入昵称"
        temp.textColor = ColorAsset.c333333.value
        return temp
    }()
    
    let signLab: UILabel = {
        let temp = UILabel()
        temp.text = "个性签名"
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level14)
        return temp
    }()
    
    let textView: HNTextView = {
        let temp = HNTextView()
        temp.placeholder = "请输入您的个性签名，签名中不能有任何\n联系方式，否则审核不过"
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level14)
        return temp
    }()
    
    lazy var  btn: UIButton = {
        let temp = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: kSCREEN_WIDTH-40, height: 45))
        temp.setTitle("确定", for: .normal)
        temp.setTitleColor(.white, for: .normal)
        temp.gradient()
        temp.addTarget(self, action: #selector(click(sender: )), for: .touchUpInside)
        return temp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "修改资料"
        
        self.view.addSubview(nameLib)
        self.view.addSubview(textInput)
        self.view.addSubview(signLab)
        self.view.addSubview(textView)
        self.view.addSubview(btn)
        
        nameLib.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo((self.navigationBar as! UIView).height)
        }
        
        textInput.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.height.equalTo(35)
            make.right.equalTo(-15)
            make.top.equalTo(nameLib.snp.bottom).offset(10)
        }
        
        signLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(textInput.snp.bottom).offset(20)
        }
        
        textView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(signLab.snp.bottom).offset(10)
            make.right.equalTo(-15)
            make.height.equalTo(250)
        }
        btn.snp.makeConstraints { (make) in
            make.left.equalTo(20)
            make.right.equalTo(-20)
            make.height.equalTo(45)
            make.bottom.equalTo(-kBottom_HEIGHT-10)
        }
    }
    
    @objc func click(sender: UIButton)
    {
        if textView.text.count == 0 {
            self.view.makeToast("请输入个性签名")
            return
        }
        if textInput.text?.count == 0 {
            self.view.makeToast("请输入昵称")
            return
        }
        
        sender.showIndicator()
        let deadline = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: {
            UserInfo.default.nickname = self.textInput.text ?? ""
            UserInfo.default.signature = self.textView.text ?? ""
            self.navigationController?.popViewController(animated: true)
            sender.hideIndicator()
        })
    }
    

}
