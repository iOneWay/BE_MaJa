//
//  ArticleDetailVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/30.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class ArticleDetailVC: BaseViewController {

    var model: ArticleModel? {
        didSet {
            bottomV.id = model?.id
        }
    }
    var image = UIImage()
    
    let headView: ArticleDetailHeadView = {
        let temp = ArticleDetailHeadView.init(frame: CGRect.init(x: 0, y: 0, width: kSCREEN_WIDTH, height: isiPhoneX() ? 313 : 290))
        temp.gradient()
        return temp
    }()
    
    let timeLab: UILabel = {
        let temp = UILabel()
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level18)
        temp.text = ""
        return temp
    }()
    
    let seperateLineV: UIView = {
        let temp = UIView()
        temp.backgroundColor = ColorAsset.ceeeeee.value
        return temp
    }()
    
    let textLab: UILabel = {
        let temp = UILabel()
        temp.text = ""
        temp.font = FontAsset.PingFangSC_Medium.size(.Level15)
        temp.numberOfLines = 0
        return temp
    }()
    let bottomV: ArticleBottom = {
        let temp = ArticleBottom()
        
        return temp
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let bar = self.navigationBar as? UIView {
            bar.backgroundColor = UIColor.clear
            bar.removeGradient()
        }
        
        _setupUI()
        
        if let likes = model?.likes {
            headView.likeLab.text = "\(likes)点赞"
        }else {
            headView.likeLab.text = "\(0)点赞"
        }
        
        headView.readLab.text = "\(arc4random()%3000)浏览"
        timeLab.text = model?.created_at ?? ""
        textLab.text = model?.content ?? ""
        
    }
    
    func _setupUI()
    {
        self.view.addSubview(headView)
        self.view.addSubview(timeLab)
        self.view.addSubview(textLab)
        self.view.addSubview(seperateLineV)
        self.view.sendSubviewToBack(headView)
        self.view.addSubview(bottomV)
        headView.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.top.equalToSuperview().offset(0)
            make.height.equalTo(isiPhoneX() ? 313 : 290)
        }
        
        timeLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(headView.snp.bottom).offset(10)
            make.height.equalTo(15)
        }
        
        seperateLineV.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(1)
            make.top.equalTo(timeLab.snp.bottom)
        }
        
        textLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(timeLab.snp.bottom).offset(10)
            
        }
        bottomV.snp.makeConstraints { (make) in
            make.left.right.equalToSuperview()
            make.height.equalTo(60)
            make.bottom.equalToSuperview().offset(-kBottom_HEIGHT-10)
        }
    }
}


class ArticleDetailHeadView: UIView {
    let icoView: UIImageView = {
        let temp = UIImageView()
        temp.image = ImageAsset.ico_person_pic.value
        temp.clipsToBounds = true
        return temp
    }()
    
    let likeLab: UILabel = {
        let temp = UILabel()
        temp.textColor = UIColor.white
        temp.font = FontAsset.PingFangSC_Medium.size(.Level21)
        temp.text = "0点赞"
        return temp
    }()
    
    let readLab: UILabel = {
        let temp = UILabel()
        temp.textColor = UIColor.white
        temp.font = FontAsset.PingFangSC_Medium.size(.Level21)
        temp.text = "0浏览"
        return temp
    }()
    
    let nameLab: UILabel = {
        let temp = UILabel()
        temp.textColor  = UIColor.white
        temp.text = ""
        temp.font = FontAsset.PingFangSC_Medium.size(.Level16)
        return temp
    }()
    
    
   
    let seperateLine: UIView = {
        let temp = UIView()
        temp.backgroundColor = UIColor.white
        return temp
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        _setupUI()
         _cons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func _setupUI() {
        self.addSubview(icoView)
        self.addSubview(readLab)
        self.addSubview(nameLab)
        self.addSubview(likeLab)
        self.addSubview(seperateLine)
       
    }
    
    func _cons() {
        icoView.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.height.equalTo(94)
            make.top.equalTo(43)
        }
        
        nameLab.snp.makeConstraints { (make) in
            make.centerX.equalTo(icoView)
            make.top.equalTo(icoView.snp.bottom)
        }
        
        likeLab.snp.makeConstraints { (make) in
            make.centerX.equalTo(kSCREEN_WIDTH/4.0)
            make.bottom.equalTo(-32)
        }
        
        readLab.snp.makeConstraints { (make) in
            make.centerX.equalTo(kSCREEN_WIDTH/4.0*3.0)
            make.bottom.equalTo(-32)
        }
        
        seperateLine.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.width.equalTo(1)
            make.height.equalTo(30)
            make.centerY.equalTo(readLab)
        }
        
        
    }
}


class ArticleBottom: UIView, UIActionSheetDelegate {
    var id: Int?
    lazy var likeV: LLButton = {
        let temp = LLButton()
        temp.imageAlignment = .top
        temp.spaceBetweenTitleAndImage = 5
        temp.setTitleColor(ColorAsset.c333333.value, for: .normal)
        temp.setImage(ImageAsset.ico_unlike.value, for: .normal)
        temp.setImage(ImageAsset.ico_like.value, for: .selected)
        temp.titleLabel?.font = FontAsset.PingFangSC_Regular.size(.Level12)
        temp.addTarget(self , action: #selector(likeVClick(sender: )), for: .touchUpInside)
        temp.setTitle("点赞", for: .normal)
        return temp
    }()
    
    lazy var storeV: LLButton = {
        let temp = LLButton()
        temp.imageAlignment = .top
        temp.spaceBetweenTitleAndImage = 5
        temp.titleLabel?.font = FontAsset.PingFangSC_Regular.size(.Level12)
        temp.setTitleColor(ColorAsset.c333333.value, for: .normal)
        temp.setImage(ImageAsset.ico_store.value, for: .normal)
        temp.setImage(ImageAsset.ico_unstore.value, for: .selected)
        temp.setTitle("收藏", for: .normal)
        temp.addTarget(self , action: #selector(storeVClick(sender: )), for: .touchUpInside)
        return temp
    }()
    
    lazy var reportV: LLButton = {
        let temp = LLButton()
        temp.imageAlignment = .top
        temp.spaceBetweenTitleAndImage = 5
        temp.setTitleColor(ColorAsset.c333333.value, for: .normal)
        temp.setImage(ImageAsset.report.value, for: .normal)
        temp.titleLabel?.font = FontAsset.PingFangSC_Regular.size(.Level12)
        temp.setTitle("举报", for: .normal)
        temp.addTarget(self , action: #selector(reportClick(sender: )), for: .touchUpInside)
        return temp
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addSubview(likeV)
        self.addSubview(storeV)
        self.addSubview(reportV)
        
        
        _cons()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func _cons()
    {
        likeV.snp.makeConstraints { (make) in
            make.left.equalTo(35.x)
            make.centerY.equalToSuperview()
            make.width.equalTo(60)
        }
        
        storeV.snp.makeConstraints { (make) in
            make.center.equalToSuperview()
            make.width.equalTo(60)
        }
        
        reportV.snp.makeConstraints { (make) in
            make.right.equalToSuperview().offset(-40.x)
            make.centerY.equalToSuperview()
            make.width.equalTo(60)
        }
        
    }
    
    
    @objc func reportClick(sender: UIButton) {
        let alertSheet = UIActionSheet(title: "举报", delegate: self as! UIActionSheetDelegate, cancelButtonTitle: "取消", destructiveButtonTitle: nil, otherButtonTitles: "广告", "低俗内容")
        alertSheet.show(in: AppDelegate.default.window ?? UIView())

    }
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        let buttonTitle = actionSheet.buttonTitle(at: buttonIndex)
        if buttonTitle == "取消"
        {
           
        }
        else if buttonTitle == "广告"
        {
            
        }
        else
        {
           
        }
    }
    
    @objc func storeVClick(sender: UIButton) {
        sender.isSelected = !sender.isSelected
        var aid = 0
        if let mid = self.id {
            aid = mid
        }
        
        MMProvider.request(.Diary(.like(id: "\(aid)", type:"collection" )), completion: {[weak self] result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                           
                            
                        }else {
                            
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
    
    @objc func likeVClick(sender: UIButton) {
         sender.isSelected = !sender.isSelected
        var aid = 0
        if let mid = self.id {
            aid = mid
        }
        MMProvider.request(.Diary(.like(id: "\(aid)", type: "like")), completion: {[weak self] result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                            
                            
                            
                        }else {
                            
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
}

