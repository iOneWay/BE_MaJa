//
//  settingVC.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class settingVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    fileprivate var listTableView = UITableView(frame: CGRect.zero, style: .plain)
    fileprivate var arraytitle = ["帮助","反馈问题"]

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.navigationBar?.leftImg = UIImage()
        self.title = "系统设置"
        initUI()

        // Do any additional setup after loading the view.
    }
    fileprivate func initUI(){
        self.listTableView.estimatedRowHeight = 100
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.showsVerticalScrollIndicator = false
        self.listTableView.separatorStyle = .none
        
        self.listTableView.tableFooterView = UIView()
        self.listTableView.backgroundColor = .clear
        
        self.listTableView.register(SettingCell.self, forCellReuseIdentifier: SettingCell.className)
        
        self.view.addSubview(self.listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(kNavi_HEIGHT)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
        
        let loginOut = UIButton()
        loginOut.setBackgroundImage(ImageAsset.mine_loginOut.value, for: .normal)
        self.view.addSubview(loginOut)
        loginOut.snp.makeConstraints { (make) in
            make.bottom.equalToSuperview().offset(-64)
            make.left.equalTo(16)
            make.right.equalTo(-16)
            make.height.equalTo(47)
            
        }
        
        loginOut.addTarget(self, action: #selector(logoutClick(sender:)), for: .touchUpInside)
    }
    
    @objc func logoutClick(sender: UIButton) {
        sender.showIndicator()
        let deadline = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: { [weak self] in
            sender.hideIndicator()
            UserInfo.default.token = ""
            self?.navigationController?.popViewController(animated: true)
            AppDelegate.default.setRootVC()
        })
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SettingCell.className, for: indexPath) as? SettingCell
         cell?.setLeftTitle(arraytitle[indexPath.row + indexPath.section * 2] )
        return cell ?? SettingCell();
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView = UIView()
        headView.backgroundColor = ColorAsset.ceeeeee.value
        return headView
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
        let type = indexPath.row

        switch type {
    
        case 0:
            self.navigationController?.pushViewController(HelpingVC(), animated: true)
            
            break
        case 1:
            self.navigationController?.pushViewController(QuestionVC(), animated: true)
            
            break

        default:
            break
        }
        
        
        
    }
    
    
    
    
}
