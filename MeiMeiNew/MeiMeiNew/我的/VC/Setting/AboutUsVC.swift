//
//  AboutUsVC.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/29.
//  Copyright © 2019 adm1in. All rights reserved.
//

import UIKit

class AboutUsVC: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      self.title = "关于我们"
        
        let logo = UIImageView(image: ImageAsset.Mine_logo.value)
        self.view.addSubview(logo)
        logo.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(100)
            make.centerX.equalToSuperview()
            make.width.height.equalTo(96)
        }
        let checkLabel = UILabel()
        checkLabel.font = UIFont.systemFont(ofSize: 12)
        checkLabel.text = "MeiMei日记 v1.00"
        checkLabel.textColor = ColorAsset.c999999.value
        
        self.view.addSubview(checkLabel)
        checkLabel.snp.makeConstraints { (make) in
            make.top.equalTo(logo.snp.bottom).offset(13)
            make.height.equalTo(17)
            make.centerX.equalToSuperview()
        }
        
        let checkBT = UIButton()
        checkBT.setBackgroundImage(ImageAsset.mine_AboutUs_checek.value, for: .normal)
        self.view.addSubview(checkBT)
        checkBT.snp.makeConstraints { (make) in
            make.top.equalTo(checkLabel.snp.bottom).offset(40)
            make.centerX.equalToSuperview()
            make.height.equalTo(36)
            make.width.equalTo(125)
        }
        
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
