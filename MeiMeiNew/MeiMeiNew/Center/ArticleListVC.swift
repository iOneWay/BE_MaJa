//
//  ArticleListVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

enum ArticleType: String {
    case new
    case hot
    case my
    case dustbin
    case collection
}


class ArticleListVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ArticleCell = tableView.dequeue(indexPath: indexPath)
        cell.model = self.itemList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ArticleDetailVC()
        vc.model = self.itemList[indexPath.row]
        let cell: ArticleCell = tableView.dequeue(indexPath: indexPath)
        vc.image = cell.icoView.image ?? UIImage()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    private lazy var _tableView: UITableView = {
        let temp = UITableView()
        temp.delegate = self
        temp.dataSource = self
        temp.registerClass(ArticleCell.self)
        temp.tableFooterView = UIView()
        temp.separatorStyle = .none
        temp.rowHeight = 131
        return temp
    }()
    
    var type: ArticleType = .new
    var itemList: [ArticleModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.type == .collection {
            self.title = "我的收藏"
        }
        if self.type == .dustbin {
            self.title = "垃圾箱"
        }
        self.navigationBar?.hide = true
        _setupUI()
        _loadData()
        
    }
    
    private func _setupUI() {
        self.view.addSubview(_tableView)
        _tableView.snp.makeConstraints { (make) in
            make.top.equalTo(0)
            make.left.right.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
    

    
    private func _loadData() {
        MMProvider.request(.Diary(.list(type: type.rawValue, kw: "")), completion: {[weak self] result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                            self?.itemList = repos.data
                            self?._tableView.reloadData()
                            
                        }else {
                            self?.view.makeToast(repos.msg, position: .center)
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
}

class ArticleCell: UITableViewCell {
    var model: ArticleModel? {
        didSet {
            self.titleLab.text = model?.title
            self.subTextLab.text = model?.content
            self.timeLab.text = model?.created_at
//            self.icolikeView = model?.likes
            self.icoView.image = UIImage.init(named: "pic_\(arc4random()%3)")
        }
    }
    
    let bgView: UIView = {
        let temp = UIView()
        temp.backgroundColor = UIColor.white
        return temp
    }()
    
    let icoView: UIImageView = {
        let temp = UIImageView()
        temp.image = UIImage()
        temp.layer.cornerRadius = 22
        temp.clipsToBounds = true
        return temp
    }()
    
    let titleLab: UILabel = {
        let temp = UILabel()
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level14)
        
        return temp
    }()
    
    let timeLab: UILabel = {
        let temp = UILabel()
        temp.textColor = ColorAsset.c999999.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level12)
        
        return temp
    }()
    
    let subTextLab: UILabel = {
        let temp = UILabel()
        temp.text = ""
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level12)
        return temp
    }()
    
    lazy var icolikeView: UIButton = {
        let temp = UIButton()
        temp.setImage(ImageAsset.ico_unlike.value, for: .normal)
        temp.setImage(ImageAsset.ico_like.value, for: .selected)
        temp.addTarget(self, action: #selector(click(sender:)), for: .touchUpInside)
        return temp
    }()
    
    @objc func click(sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.contentView.backgroundColor = ColorAsset.ceeeeee.value
        self.backgroundColor = ColorAsset.ceeeeee.value
        self.selectionStyle = .none
        _setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func _setupUI() {
        self.contentView.addSubview(bgView)
        bgView.addSubview(icoView)
        bgView.addSubview(titleLab)
        bgView.addSubview(timeLab)
        bgView.addSubview(subTextLab)
        bgView.addSubview(icolikeView)
        
        _cons()
    }
    
    func _cons() {
        
        bgView.snp.makeConstraints { (make) in
            make.top.right.left.equalToSuperview()
            make.bottom.equalToSuperview().offset(-20)
        }
        icoView.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.top.equalTo(13)
            make.width.height.equalTo(44)
        }
        
        titleLab.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.left.equalTo(icoView.snp.right).offset(10)
        }
        
        timeLab.snp.makeConstraints { (make) in
            make.top.equalTo(titleLab.snp.bottom).offset(5)
            make.left.equalTo(titleLab.snp.left)
        }
        
        subTextLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(icoView.snp.bottom).offset(5)
            make.right.equalTo(-15)
        }
        
        icolikeView.snp.makeConstraints { (make) in
            make.right.equalTo(-17)
            make.top.equalTo(15)
        }
    }
    
}
