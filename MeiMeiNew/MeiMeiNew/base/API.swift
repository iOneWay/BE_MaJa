//
//
//  Created by 王伟 on 2018/9/2.
//  Copyright © 2017年 王伟. All rights reserved.
//

import Foundation
import Moya
import Alamofire
import ObjectMapper


enum SMSCodeType: String {
    case register
    case login
    case other
    case weixin
}


enum API {
    case Login(LoginAPI)     //登录模块
    case Diary(DiaryAPI)     //登录模块
}

extension API: TargetType {
 
    var serverUrl: String{
//        let url = "http://192.168.1.254:8090"
        let  url =  "http://note.meimei.im"
        return url
    }
    
    var baseURL: URL {

            return URL(string: serverUrl)!
    }
    
    var path: String {
        switch self {
            case .Login(let login):
                return login.path
        case .Diary(let module):
            return module.path
            
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .Login(let login):
            return login.method
        case .Diary(let module):
            return module.method
            
        }
    }
    
    var sampleData: Moya.Data {
        return "".data(using: String.Encoding.utf8)!
    }
    
    var task: Task {
        switch self {
            case .Login(let login):
                return login.task
            case .Diary(let module):
                return module.task
        }
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"];
    }
    
    
    static var baseParameter: [String : String]? {
        return [
                "os": "IOS",
                "imsi": App.imsi.imsi,
                "imei": App.IDFV,
                "device": App.deviceName,
                "app_version": App.appVersion,
                "os_version":App.sysVersion,
                "channel": "5",
                "token": UserInfo.default.token
        ];
    }
    
}


private func JSONResponseDataFormatter(_ data: Data) -> Data {
    do {
        let dataAsJSON = try JSONSerialization.jsonObject(with: data)
        let prettyData =  try JSONSerialization.data(withJSONObject: dataAsJSON, options: .prettyPrinted)
        return prettyData
    } catch {
        return data
    }
}



//接收响应
class ResponseModel: Mappable, CustomDebugStringConvertible{
    var result: Int = 1
    var msg: String = ""
    var data: [String: String] = [:]
    
    var debugDescription: String {
        return """
        ResponseModel：{
            result: \(String(describing: result))
            msg: \(String(describing: msg))
        }
        """
    }
    
    
    required init?(map: Map){}
    init() {}
    
    func mapping(map: Map)
    {
        msg <- map["msg"]
        result <- map["result"]
        data <- map["data"]
    }
}

class ResponseLoginModel: Mappable, CustomDebugStringConvertible{
    var result: Int = 1
    var msg: String = ""
    var data: Login2Model = Login2Model()
    
    var debugDescription: String {
        return """
        ResponseModel：{
        result: \(String(describing: result))
        msg: \(String(describing: msg))
        }
        """
    }
    
    
    required init?(map: Map){}
    init() {}
    
    func mapping(map: Map)
    {
        msg <- map["msg"]
        result <- map["result"]
        data <- map["data"]
    }
}

class Login2Model: Mappable {
    var token: String = ""
    var user: UserModel = UserModel()
    
    required init?(map: Map){}
    init() {}
    
    func mapping(map: Map)
    {
        token <- map["token"]
        user <- map["user"]
    }
}

class UserModel: Mappable {
    var mobile: String = ""
    var name: String = ""
    
    required init?(map: Map){}
    init() {}
    
    func mapping(map: Map)
    {
        mobile <- map["mobile"]
        name <- map["name"]
    }
}

//接收响应
class ArticleResponseModel: Mappable, CustomDebugStringConvertible{
    var result: Int = 1
    var msg: String = ""
    var data: [ArticleModel] = []
    
    var debugDescription: String {
        return """
        ResponseModel：{
        result: \(String(describing: result))
        msg: \(String(describing: msg))
        }
        """
    }
    
    
    required init?(map: Map){}
    init() {}
    
    func mapping(map: Map)
    {
        msg <- map["msg"]
        result <- map["result"]
        data <- map["data"]
    }
}

class ArticleModel: Mappable, CustomDebugStringConvertible {
    var id: Int = 0
    var user_id: Int = 0
    var title: String = ""
    var content: String = ""
    var likes: Int = 0
    var created_at = ""
    var updated_at = ""
    var status = 1
    
    required init?(map: Map){}
    init() {}
    
    func mapping(map: Map)
    {
        id <- map["id"]
        user_id <- map["user_id"]
        title <- map["title"]
        content <- map["content"]
        likes <- map["likes"]
        created_at <- map["created_at"]
        status <- map["status"]
        updated_at <- map["updated_at"]
    }
    
    var debugDescription: String {
        return """
        ResponseModel：{
        id: \(String(describing: id))
        user_id: \(String(describing: user_id))
        title: \(String(describing: title))
        content: \(String(describing: content))
        likes: \(String(describing: likes))
        created_at: \(String(describing: created_at))
        }
        """
    }
}

let MMProvider = MoyaProvider<API>(plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
