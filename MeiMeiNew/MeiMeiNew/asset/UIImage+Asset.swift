//
//  UIImage.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/23.
//  Copyright © 2019 admin. All rights reserved.
//

import Foundation
import UIKit

typealias ImageAsset = UIImage.Asset

extension UIImage {
    public enum Asset: String {
        case lunch_bg
        case logo
        case login_bg
        case phon_login
        case pwd_login
        case nv_back
        case tab_jsb_sel
        case tab_jsb
        case tab_person_sel
        case tab_person
        case edit
        case search
        case diary_line
        case diary_content_bg
        case mine_helping
        
        
        case mine_headback
        case mine_titleImage
        case mine_collectHead1
        case mine_collectHead2
        case mine_collectHead3
        case mine_collectHead4

        case Mine_logo
        case mine_loginOut
        case mine_AboutUs_checek
        
        case mine_laji
        case mine_nextPage
        case mine_tongzhi
        case mine_shezhi
        case mine_xiugai
        case mine_shoucang
        case ic_delete
        case ico_search_field
        case ico_nav_add
        case ico_like
        case ico_unlike
        
        case ico_person_pic
        case ico_unstore
        case ico_store
        case report
        
        case ic_list_pic
        
        case pic_0
        case pic_1
        case pic_2
        case pic_3
        
        case ic_center_sel
        case ic_center

        var value: UIImage? {
            return UIImage.init(named: self.rawValue)
        }
    }
}
