//
//  CenterVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class CenterVC: BaseViewController, BEMenuViewDelegate {
    internal lazy var contentScrollView: UIScrollView = {
//        $0.isPagingEnabled = true
        $0.isDirectionalLockEnabled = true
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.scrollsToTop = false
        $0.bounces = false
        $0.delegate = self
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIScrollView(frame: .zero))
    
    internal let contentView = UIView()
    var menuView: BEMenuView?
    var selectIndex = 0
    var lastOffSetX = CGFloat(0.0)
    var controllers: [UIViewController]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "浏览"
        self.navigationBar?.leftNavBtn?.isHidden = true
        self.title = ""
        self.menuView = BEMenuView.init(itemTexts: ["最新", "热门", "我的"], frame: CGRect.init(x: 0, y: 0, width: 270, height: 44))
        
        
        let navBar = self.navigationBar as! UIView
        navBar.addSubview(menuView!)
        menuView!.centerX = navBar.centerX
        menuView!.bottom = navBar.height
        menuView?.delegate = self
        
        
        let vc0 = ArticleListVC()
        vc0.type = .new
        vc0.view.backgroundColor = UIColor.gray
        let vc1 = ArticleListVC()
        vc1.type = .hot
        vc1.view.backgroundColor = UIColor.green
        let vc2 = ArticleListVC()
        vc2.type = .my
        vc2.view.backgroundColor = UIColor.red
        
        
        setupContentScrollView()
        self.controllers = [vc0, vc1, vc2]
        
        for (index, vc) in self.controllers!.enumerated()  {
            self.addChild(vc)
            contentView.addSubview(vc.view)
            if index == (controllers!.count - 1) {
                vc.view.snp.makeConstraints { (make) in
                    make.top.bottom.equalToSuperview()
                    make.left.equalToSuperview().offset(CGFloat(index) * kSCREEN_WIDTH)
                    make.width.equalTo(kSCREEN_WIDTH)
                    make.right.equalTo(0)
                }
            }else {
                vc.view.snp.makeConstraints { (make) in
                    make.top.bottom.equalToSuperview()
                    make.left.equalToSuperview().offset(CGFloat(index) * kSCREEN_WIDTH)
                    make.width.equalTo(kSCREEN_WIDTH)
                }
            }
            
        }
    }
    
    func scrollViewTo(index: Int) {
        self.contentScrollView.contentOffset = CGPoint.init(x: CGFloat(index)*kSCREEN_WIDTH, y: 0)
    }
    
    fileprivate func setupContentScrollView() {
        
        view.addSubview(contentScrollView)
        
        contentScrollView.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(view)
            make.top.equalTo(view).offset(kNavi_HEIGHT)
        }
        
        contentScrollView.addSubview(contentView)
        
        contentView.snp.makeConstraints { (make) in
            make.top.equalTo(view).offset(kNavi_HEIGHT)
            make.bottom.equalTo(view)
            make.left.right.equalToSuperview()
        }
        
    }
}

extension CenterVC: UIScrollViewDelegate {
    public func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if self.lastOffSetX < scrollView.contentOffset.x { //向左滚动
            if Int(scrollView.contentOffset.x) >= self.selectIndex * Int(kSCREEN_WIDTH) + Int(kSCREEN_WIDTH/2.0 - 50) { //向左滚动
                
                self.selectIndex = self.selectIndex + 1
                
                self.menuView?.move(to: self.selectIndex)
            }
        }else {
            if Int(scrollView.contentOffset.x) <= self.selectIndex * Int(kSCREEN_WIDTH) - Int(kSCREEN_WIDTH/2.0 + 50) { //向左滚动
                
                self.selectIndex = self.selectIndex - 1
                
                self.menuView?.move(to: self.selectIndex)
            }
        }
        
        
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastOffSetX = scrollView.contentOffset.x
    }
}
