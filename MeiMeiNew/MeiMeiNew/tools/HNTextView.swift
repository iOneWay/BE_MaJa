//
//  HNTextView.swift
//  newToken_swift
//
//  Created by 王伟 on 2019/1/7.
//  Copyright © 2019 王伟. All rights reserved.
//

import UIKit

class HNTextView: UITextView , UITextViewDelegate{
    
    var placeholder : String?  {
        didSet {
            self.placeholderLabel.text = placeholder
            self.placeholderLabel.sizeToFit()
        }
    }
    var placeholderFont : UIFont?
    var placeholderColor : UIColor?
    
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        self.addSubview(placeholderLabel)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textDidBeginEditing), name: UITextView.textDidBeginEditingNotification, object: nil)
        
        //文本框编辑结束时，触发
        NotificationCenter.default.addObserver(self, selector: #selector(textDidEndEditing), name: UITextView.textDidEndEditingNotification, object: nil)
        
        
        //文本框内容改变时，触发
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange), name: UITextView.textDidChangeNotification, object: nil)
        
        
        if placeholder == nil{
            placeholder = ""
        }
        
        if placeholderFont == nil {
            
            placeholderFont = UIFont.systemFont(ofSize: 16)
            
        }
        
        if placeholderColor == nil {
            
            placeholderColor = ColorAsset.c999999.value
            
        }
        
        
        let fontSize = CGSize(width: self.width - 16, height: self.height - 10)
        
        let placeSize:CGSize = placeholder!.boundingRect(with: fontSize, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: placeholderFont!], context: nil).size;
        
        
        placeholderLabel.frame = CGRect(x:8, y:5, width:placeSize.width ,height:placeSize.height + 1)
        placeholderLabel.text = placeholder!
        placeholderLabel.font = placeholderFont!
        placeholderLabel.textColor = placeholderColor!
        
        
    }
    
    @objc  func textDidBeginEditing(){
        
        //print("textDidBeginEditing")
        
    }
    
    @objc func textDidEndEditing(){
        //print("textDidEndEditing")
    }
    
    
    @objc func textDidChange(){
        
        if self.text.isEmpty {
            
            placeholderLabel.isHidden = false
        }else{
            placeholderLabel.isHidden = true
        }
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    lazy var  placeholderLabel:UILabel = {
        let placeholderLabel = UILabel()
        placeholderLabel.numberOfLines = 0
        return placeholderLabel
    }()
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
}

