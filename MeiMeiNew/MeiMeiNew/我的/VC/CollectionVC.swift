//
//  CollectionVC.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class CollectionVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    fileprivate var listTableView = UITableView(frame: CGRect.zero, style: .plain)
    fileprivate var array = [ImageAsset.mine_collectHead1.value,ImageAsset.mine_collectHead2.value,ImageAsset.mine_collectHead3.value,ImageAsset.mine_collectHead4.value,ImageAsset.mine_shezhi.value] as [Any]
    
    fileprivate var nameArray = ["我爱你呦","绿茶婊","小强来了","摸摸哒","比 u比 u"]
    fileprivate var timeArray = ["2018 07 01 13:33","2019 03 05 11:33","2017 10 22 12:23","2019 05 12 15:20","2019 02 23 09:19"]
    fileprivate var singalArray = ["人生就是一只储蓄罐，你投入的每一分努力，都会在未来的某一天，打包还给你。别人拥有的，你只要愿意去付出，一样可以拥有。","花季的烂漫，雨季的忧伤，随着年轮渐渐淡忘，沉淀于心的，一半是对美好的追求，一半是对残缺的接纳。你的脆弱和坚强往往都超乎自己的想象。","生活是没有捷径的，它考验的是你的恒心与耐力，只要你多坚持一下，不荒废现在，不惧怕未来，一切就会在你的掌握之中！","不管当下的我们有没有人爱，我们也要努力做一个可爱的人。不埋怨谁，不嘲笑谁，也不羡慕谁，阳光下灿烂，风雨中奔跑，做自己的梦，走自己的路。","如果你不相信努力和时光，那么时光第一个就会辜负你。不要去否定你的过去，也不要用你的过去牵扯你的未来。不是因为有希望才去努力，而是努力了，才能看到希望。"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar?.leftImg = ImageAsset.nv_back.value
        self.title = "我的收藏"

        initUI()
    }
    
    fileprivate func initUI(){
//        self.listTableView.estimatedRowHeight = 300
        
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.showsVerticalScrollIndicator = false
        self.listTableView.separatorStyle = .none
        
        self.listTableView.tableFooterView = UIView()
        self.listTableView.backgroundColor = .clear
        
        self.listTableView.register(CollectionCell.self, forCellReuseIdentifier: CollectionCell.className)
         self.view.addSubview(self.listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(kNavi_HEIGHT)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CollectionCell.className, for: indexPath) as? CollectionCell
        
        cell?.setImageData(array[indexPath.row] as! UIImage)
        cell?.setNameLabel(nameArray[indexPath.row])

        cell?.setSingalLabel(singalArray[indexPath.row])
        cell?.setTimeLabel(timeArray[indexPath.row])

        
         return cell ?? CollectionCell();
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 116
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
      }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView = UIView()
        headView.backgroundColor = ColorAsset.ceeeeee.value
        return headView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
