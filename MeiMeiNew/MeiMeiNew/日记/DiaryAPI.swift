//
//  LoginAPI.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/25.
//  Copyright © 2019 admin. All rights reserved.
//

import Foundation
import Moya
//首页

enum DiaryAPI{
    case list(type: String, kw: String)
    case save(content: String, id: String, status: String)
    case like(id: String, type: String)
}

extension DiaryAPI{
    var path:String{
        switch self {
            case .list(_, _):
                return "/api/article/list"
            case .save(_, _, _):
                return "/api/article/save"
            case .like(_, _):
                return "/api/article/like"
        }
    }
    
    var method: Moya.Method{
        return .get
    }
    
    var task:Task{
        switch self {
            case .list(let type, let kw):
                return .requestParameters(parameters: ["token":UserInfo.default.token  ,"type":type, "kw":kw] , encoding: URLEncoding.default)
            case .save(let content, let id, let status):
                return .requestParameters(parameters: ["token":UserInfo.default.token  ,"content":content, "status":status, "id": id] , encoding: URLEncoding.default)
            case .like(let id, let type):
                return .requestParameters(parameters: ["id":id, "type": type] , encoding: URLEncoding.default)
        }
        
    }
}

