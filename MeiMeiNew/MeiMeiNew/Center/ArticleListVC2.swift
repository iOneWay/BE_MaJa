//
//  ArticleListVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit


class ArticleListVC2: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: ArticleCell = tableView.dequeue(indexPath: indexPath)
        cell.model = self.itemList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = ArticleDetailVC()
        vc.model = self.itemList[indexPath.row]
        let cell: ArticleCell = tableView.dequeue(indexPath: indexPath)
        vc.image = cell.icoView.image ?? UIImage()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    private lazy var _tableView: UITableView = {
        let temp = UITableView()
        temp.delegate = self
        temp.dataSource = self
        temp.registerClass(ArticleCell.self)
        temp.tableFooterView = UIView()
        temp.separatorStyle = .none
        temp.rowHeight = 131
        return temp
    }()
    
    var type: ArticleType = .new
    var itemList: [ArticleModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.type == .dustbin {
            self.title = "垃圾箱"
        }else {
            self.title = "我的收藏"
        }
        
        _setupUI()
        _loadData()
        
    }
    
    private func _setupUI() {
        self.view.addSubview(_tableView)
        _tableView.snp.makeConstraints { (make) in
            make.top.equalTo(kNavi_HEIGHT)
            make.left.right.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
    

    
    private func _loadData() {
        MMProvider.request(.Diary(.list(type: type.rawValue, kw: "")), completion: {[weak self] result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                            self?.itemList = repos.data
                            self?._tableView.reloadData()
                            
                        }else {
                            self?.view.makeToast(repos.msg, position: .center)
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
}
