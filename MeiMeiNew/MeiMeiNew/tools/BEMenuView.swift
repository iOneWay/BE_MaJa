//
//  MenuView.swift
//  MeiMeiNew
//
//  Created by 王伟 on 2019/5/26.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit
import SnapKit

protocol BEMenuViewDelegate {
    func scrollViewTo(index: Int)
}

class BEMenuView: UIView {
    
    var delegate: BEMenuViewDelegate?
    private var _line: UIView = {
        let temp = UIView()
        temp.height = 2
        temp.backgroundColor = UIColor.black
        return temp
    }()
    fileprivate var currentIndex: Int = 0
    internal fileprivate(set) var menuItemViews = [BEItemView]()
    
    fileprivate let contentView: UIView = {
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIView(frame: .zero))
    
    override init(frame:CGRect){
        super.init(frame: frame)
    }
    
    internal init(itemTexts: [String], frame: CGRect) {
        super.init(frame: frame)
        itemTexts.forEach { (text) in
           
        }
        
        for (index, text) in itemTexts.enumerated() {
            let itemView = BEItemView.init(menuText: MenuItemText.init(text: text))
            itemView.tag = index
            self.menuItemViews.append(itemView)
            itemView.addOnClickListener(target: self, action: #selector(itemClick(sender:)))
        }
        
        commonInit()
    }
    
    @objc func itemClick(sender:UITapGestureRecognizer) {
        self.move(to: sender.view?.tag ?? 0)
        if let delegate = delegate {
            self.delegate?.scrollViewTo(index: sender.view?.tag ?? 0)
        }
    }
    
    fileprivate func commonInit() {
//        setupScrollView()
        setupContentView()
        layoutItems()
        setupUnderlineViewIfNeeded()
    }
    
    fileprivate func layoutItems() {
        
        let count = menuItemViews.count
        let width = self.width/CGFloat(count)
        for (index, item) in menuItemViews.enumerated() {
            contentView.addSubview(item)
            item.snp.makeConstraints({ (make) in
                make.left.equalTo(CGFloat(0.0) + CGFloat(index) * width)
                make.width.equalTo(width)
                make.top.equalTo(0)
                make.bottom.equalTo(-_line.height)
            })
        }
    }
    
    fileprivate func setupUnderlineViewIfNeeded() {
        
        contentView.addSubview(_line)
        
        let itemview = menuItemViews[currentIndex]
        _line.snp.makeConstraints { (make) in
            make.centerX.equalTo(itemview)
            make.bottom.equalTo(0)
            make.height.equalTo(2)
            make.width.equalTo(itemview.textWidth)
        }
    }
    
    
    
    
    public func move(to index: Int){
        if index >= menuItemViews.count {
            return
        }
        UIView.animate(withDuration: 0.5, animations: {[weak self] in
            if let view = self?.menuItemViews[index] {
                self?._line.snp.remakeConstraints({ (make) in
                    make.centerX.equalTo(view)
                    make.bottom.equalTo(0)
                    make.height.equalTo(2)
                    make.width.equalTo(view.textWidth)
                })
            }
        })
    }
    
    
    fileprivate func setupContentView() {
        addSubview(contentView)
        contentView.snp.makeConstraints { (make) in
            make.top.bottom.right.left.equalToSuperview()
            make.height.equalToSuperview()
        }
    }
    
    
//    fileprivate func setupScrollView() {
//        showsHorizontalScrollIndicator = false
//        showsVerticalScrollIndicator = false
//        bounces = false
//        isScrollEnabled = true
//        isDirectionalLockEnabled = true
//        scrollsToTop = false
//        translatesAutoresizingMaskIntoConstraints = false
//    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BEMenuView: Pagable {
    var currentPage: Int {
        return currentIndex
    }
    var previousPage: Int {
        return currentPage - 1 < 0 ? menuItemViews.count - 1 : currentPage - 1
    }
    var nextPage: Int {
        return currentPage + 1 > menuItemViews.count - 1 ? 0 : currentPage + 1
    }
    func update(currentPage page: Int) {
        currentIndex = page
    }
}


protocol Pagable {
    var currentPage: Int { get }
    var previousPage: Int { get }
    var nextPage: Int { get }
    func update(currentPage page: Int)
}

extension Pagable {
    func update(currentPage page: Int) {}
}
