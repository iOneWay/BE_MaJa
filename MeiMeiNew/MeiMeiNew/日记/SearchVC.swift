//
//  SearchVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class SearchVC: BaseViewController {

    let searchField: InputField = {
        let temp = InputField.init(frame: CGRect.init(x: 0, y: 0, width: kSCREEN_WIDTH - 125, height: 28))
        temp.leftImg = ImageAsset.ico_search_field.value
        temp.clipsToBounds = true
        temp.layer.cornerRadius = 14
        temp.backgroundColor = UIColor.white
        temp.placeHolder = "请输入搜索内容"
        temp.font = FontAsset.PingFangSC_Regular.size(.Level13)
        return temp
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let bar = self.navigationBar as? UIView {
            bar.addSubview(searchField)
            searchField.snp.makeConstraints { (make) in
                make.left.equalTo(53)
                make.right.equalTo(-72)
                make.height.equalTo(28)
                make.bottom.equalTo(-8)
            }
        }
    }
    

    

}
