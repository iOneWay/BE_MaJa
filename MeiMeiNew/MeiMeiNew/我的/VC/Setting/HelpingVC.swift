//
//  HelpingVC.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class HelpingVC: BaseViewController {

    internal lazy var contentScrollView: UIScrollView = {
        $0.isPagingEnabled = true
        $0.isDirectionalLockEnabled = true
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.scrollsToTop = false
        $0.bounces = false
        $0.translatesAutoresizingMaskIntoConstraints = false
        return $0
    }(UIScrollView(frame: .zero))
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "帮助"
        
        view.addSubview(contentScrollView)
        view.sendSubviewToBack(contentScrollView)
        contentScrollView.snp.makeConstraints { (make) in
            make.bottom.left.right.equalTo(view)
            make.top.equalTo(view).offset(0)
        }
        
        let backIMg = UIImageView(image: ImageAsset.mine_helping.value)
        
        contentScrollView.addSubview(backIMg)
        backIMg.snp.makeConstraints { (make) in
            make.top.equalTo(kNavi_HEIGHT)
            make.left.equalTo(0)
            make.width.equalTo(kSCREEN_WIDTH)
            make.bottom.equalToSuperview()
            make.height.equalTo(kSCREEN_WIDTH * 1.6)
        }
     }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
