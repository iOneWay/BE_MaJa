//
//  SettingCell.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    
    
    fileprivate var LeftLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        self.selectionStyle = .none
        initUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func initUI() {
        let backView = UIView()
        backView.backgroundColor = .white
        self.contentView.addSubview(backView)
        backView.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(14)
            make.right.equalTo(-14)
        }
        
        
        
        
        LeftLabel.text = "我的收藏"
        
        self.contentView.addSubview(LeftLabel)
        LeftLabel.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(15)
            make.centerY.equalToSuperview()
            
        }
        
        let nextImage = UIImageView(image: ImageAsset.mine_nextPage.value)
        self.contentView.addSubview(nextImage)
        nextImage.snp.makeConstraints { (make) in
            make.width.equalTo(7)
            make.height.equalTo(12)
            make.right.equalToSuperview().offset(-19)
            make.centerY.equalToSuperview()
        }
        
        let lineLabel = UILabel()
        lineLabel.backgroundColor = ColorAsset.ceeeeee.value
        self.contentView.addSubview(lineLabel)
        lineLabel.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.right.bottom.equalTo(0)
        }
        
        
    }
    
    func setLeftTitle(_ balance: String) {
        
        LeftLabel.text = balance
    }
}
