//
//  QuestionVC.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class QuestionVC: BaseViewController {

    let typeLab: UILabel = {
        let temp = UILabel()
        temp.text = "反馈类型"
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Medium.size(.Level18)
        return temp
    }()
    
    let contentLab: UILabel = {
        let temp = UILabel()
        temp.text = "您的建议"
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Medium.size(.Level18)
        return temp
    }()
    
    lazy var typebtn_0: UIButton = {
        let temp = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 85, height: 30))
        temp.setTitleColor(.white, for: .normal)
        temp.titleLabel?.font = FontAsset.PingFangSC_Regular.size(.Level14)
        temp.addTarget(self, action: #selector(typebtnClick(sender:)), for: .touchUpInside)
        temp.isSelected = true
        temp.setTitle("建议", for: .normal)
        temp.gradient()
        return temp
    }()
    
    lazy var typebtn_1: UIButton = {
        let temp = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 85, height: 30))
        temp.setTitleColor(.white, for: .normal)
        temp.setTitle("错误", for: .normal)
        temp.titleLabel?.font = FontAsset.PingFangSC_Regular.size(.Level14)
        temp.addTarget(self, action: #selector(typebtnClick(sender:)), for: .touchUpInside)
        temp.isSelected = true
        temp.gradient()
        return temp
    }()
    
    lazy var typebtn_2: UIButton = {
        let temp = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 85, height: 30))
        temp.setTitleColor(.white, for: .normal)
        temp.titleLabel?.font = FontAsset.PingFangSC_Regular.size(.Level14)
        temp.addTarget(self, action: #selector(typebtnClick(sender:)), for: .touchUpInside)
        temp.setTitle("求助", for: .normal)
        temp.isSelected = true
        temp.gradient()
        return temp
    }()
    
    let textView: HNTextView = {
        let temp = HNTextView()
        temp.placeholder = "请输入反馈内容(200字以内)"
        temp.layer.borderColor = ColorAsset.c999999.value?.cgColor
        temp.layer.borderWidth = 0.5
        temp.layer.cornerRadius = 5
        temp.clipsToBounds = true
        return temp
    }()
    
    let commitBtn: UIButton = {
        let temp = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: kSCREEN_WIDTH - 40, height: 45))
        temp.gradient()
        temp.setTitle("确定", for: .normal)
        temp.layer.cornerRadius = 22.5
        temp.clipsToBounds = true
        temp.addTarget(self, action: #selector(commitBtn(sender:)), for: .touchUpInside)
        return temp
    }()
    
    @objc func typebtnClick(sender:UIButton) {
        
    }
    
    @objc func commitBtn(sender:UIButton) {
        
        if textView.text.count == 0 {
            self.view.makeToast("请先填写建议内容")
            return
        }
        
        sender.showIndicator()
        let deadline = DispatchTime.now() + 2.0
        DispatchQueue.main.asyncAfter(deadline: deadline, execute: {
            
            self.navigationController?.popViewController(animated: true)
            sender.hideIndicator()
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "建议反馈"
        self.view.addSubview(typeLab)
        self.view.addSubview(contentLab)
        self.view.addSubview(typebtn_0)
        self.view.addSubview(typebtn_1)
        self.view.addSubview(typebtn_2)
        self.view.addSubview(textView)
        self.view.addSubview(commitBtn)
        
        _cons()
    }
    
    func _cons() {
        typeLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
           
            make.top.equalTo(kNavi_HEIGHT + 15)
        }
        
        typebtn_0.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(typeLab.snp.bottom).offset(10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        typebtn_1.snp.makeConstraints { (make) in
            make.centerX.equalToSuperview()
            make.top.equalTo(typeLab.snp.bottom).offset(10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        typebtn_2.snp.makeConstraints { (make) in
            make.right.equalTo(-15)
            make.top.equalTo(typeLab.snp.bottom).offset(10)
            make.width.equalTo(80)
            make.height.equalTo(30)
        }
        
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(typebtn_0.snp.bottom).offset(15)
        }
        
        textView.snp.makeConstraints { (make) in
            make.top.equalTo(contentLab.snp.bottom).offset(15)
            make.left.equalTo(15)
            make.right.equalToSuperview().offset(-15)
            make.height.equalTo(400)
        }
        
        commitBtn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.height.equalTo(45)
            make.bottom.equalToSuperview().offset(-kBottom_HEIGHT-10)
            
        }
        
    }
    

    

}
