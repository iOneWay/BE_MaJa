//
//  DiaryVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class DiaryVC: BaseViewController {

    let timeLab: UILabel = {
        let temp = UILabel()
        temp.textColor = ColorAsset.cBD10E0.value
        temp.font = FontAsset.PingFangSC_Regular.size(.Level16)
        temp.text = ""
        return temp
    }()
    
    var model: ArticleModel?
    let seperateLineV: UIView = {
        let temp = UIView()
        temp.backgroundColor = ColorAsset.ceeeeee.value
        return temp
    }()
    
    private var textView: HNTextView = {
        let temp = HNTextView()
        temp.layer.borderColor = ColorAsset.ceeeeee.value?.cgColor
        temp.layer.borderWidth = 0.5
        temp.layer.cornerRadius = 5.0
        return temp
    }()
    
    private lazy var doneBtn: UIView = {
        let temp = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: kSCREEN_WIDTH-40, height: 45))
        temp.setTitle("确定", for: .normal)
        temp.clipsToBounds = true
        temp.layer.cornerRadius = 22.5
        temp.titleLabel?.font = FontAsset.PingFangSC_Medium.size(.Level14)
        temp.setTitleColor(.white, for: .normal)
        temp.addTarget(self, action: #selector(doneBtn(sender:)), for: .touchUpInside)
        temp.gradient()
        return temp
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MeiMei日记"
        self.navigationBar?.rightImg = ImageAsset.ic_delete.value
        self.view.addSubview(timeLab)
        self.view.addSubview(seperateLineV)
        self.view.addSubview(textView)
        self.view.addSubview(doneBtn)
        
        self.view.backgroundColor = ColorAsset.ceeeeee.value
        
        if let model = self.model {
            timeLab.text = model.created_at
            textView.text = model.content
        }else {
            timeLab.text = String().now
        }
        
        _cons()
    }
    
    override func rightNavBtnClick(sender: UIButton) {
        var id = ""
        if let mid = model?.id {
            id = "\(mid)"
        }
        MMProvider.request(.Diary(.save(content: textView.text, id: id, status: "2")), completion: { [weak self]  result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                            self?.view.makeToast(repos.msg, position: .center)
                            self?.navigationController?.popViewController(animated: true)
                            
                        }else {
                            self?.view.makeToast(repos.msg, position: .center)
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
    
    @objc func doneBtn(sender: UIButton) {
        sender.showIndicator()
        var id = ""
        if let mid = model?.id {
            id = "\(mid)"
        }
        MMProvider.request(.Diary(.save(content: textView.text, id: id, status: "1")), completion: { [weak self]  result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                            self?.view.makeToast(repos.msg, position: .center)
                            self?.navigationController?.popViewController(animated: true)
                            
                        }else {
                            self?.view.makeToast(repos.msg, position: .center)
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
    
    
    func _cons() {
        timeLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.top.equalTo(kNavi_HEIGHT+10)
        }
        
        seperateLineV.snp.makeConstraints{(make) in
            make.left.right.equalToSuperview()
            make.top.equalTo(timeLab.snp.bottom).offset(5)
        }
        
        textView.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.right.equalTo(-15)
            make.height.equalTo(200)
            make.top.equalTo(seperateLineV.snp.bottom).offset(10)
        }
        
        doneBtn.snp.makeConstraints { (make) in
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview().offset(-kBottom_HEIGHT-10)
            make.height.equalTo(45)
        }
    }
    
    

}
