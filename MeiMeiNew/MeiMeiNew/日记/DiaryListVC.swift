//
//  DiaryVC.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class DiaryListVC: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: DiaryCell = tableView.dequeue(indexPath: indexPath)
        cell.model = itemList[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = DiaryVC()
        vc.model = itemList[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    private lazy var _tableView: UITableView = {
        let temp = UITableView()
        temp.delegate = self
        temp.dataSource = self
        temp.registerClass(DiaryCell.self)
        temp.tableFooterView = UIView()
        temp.separatorStyle = .none
        temp.rowHeight = 106
        return temp
    }()
    
    var itemList: [ArticleModel] = []
    
    override func rightNavBtnClick(sender: UIButton) {
        self.navigationController?.pushViewController(SearchVC(), animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "MeiMei日记"
        self.navigationBar?.leftImg = ImageAsset.edit.value!
//        self.navigationBar?.rightImg = ImageAsset.search.value!
        self.view.backgroundColor = ColorAsset.ceeeeee.value
        self._tableView.backgroundColor = ColorAsset.ceeeeee.value
        _setupUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        _loadData()
    }
   override func leftNavBtnClick(sender: UIButton) {
        self.navigationController?.pushViewController(DiaryVC(), animated: true)
    }
    
    private func _setupUI() {
        self.view.addSubview(_tableView)
        _tableView.snp.makeConstraints { (make) in
            make.top.equalTo((self.navigationBar as! UIView).height)
            make.left.right.equalTo(0)
            make.bottom.equalTo(0)
        }
    }
    
    private func _loadData() {
        MMProvider.request(.Diary(.list(type: "my", kw: "")), completion: {[weak self] result in
            switch result{
            case let .success(response):
                do {
                    let repos: ArticleResponseModel? = ArticleResponseModel(JSON: try response.mapJSON() as! [String : Any])
                    
                    if let repos = repos {
                        if repos.result == 1 {
                            
                            self?.itemList = repos.data
                            self?._tableView.reloadData()
                            
                        }else {
                            self?.view.makeToast(repos.msg, position: .center)
                        }
                    } else {}
                } catch {
                    
                }
            case let .failure(error):
                
                guard let description = error.errorDescription else {
                    break
                }
                print(description)
            }
        })
    }
}


class DiaryCell: UITableViewCell {
    
    var model: ArticleModel? {
        didSet {
            self.contentLab.text = model?.content
            self.weekLab.text = "周\(model?.created_at.weekDay ?? "1")"
            self.dayLab.text = "\(model?.created_at.monthDay ?? "")"
            self.timeLab.text = "\(model?.created_at.yearMonth ?? "")"
        }
    }
    
    let dayLab: UILabel = {
        let temp = UILabel()
        temp.text = "27"
        temp.font = FontAsset.PingFangSC_Medium.size(.Level17)
        temp.textColor = ColorAsset.c333333.value
        return temp
    }()
    
    let timeLab: UILabel = {
        let temp = UILabel()
        temp.text = "2019.05"
        temp.font = FontAsset.PingFangSC_Medium.size(.Level12)
        temp.textColor = ColorAsset.c999999.value
        return temp
    }()
    
    let weekLab: UILabel = {
        let temp = UILabel()
        temp.text = "周一"
        temp.font = FontAsset.PingFangSC_Medium.size(.Level14)
        temp.textColor = ColorAsset.c333333.value
        return temp
    }()
    
    let imageV: UIView = {
        let temp = UIImageView()
        temp.image = ImageAsset.diary_content_bg.value
        return temp
    }()
    
    let lineV: UIView = {
        let temp = UIImageView()
        temp.image = ImageAsset.diary_line.value
        return temp
    }()
    
    let contentLab: UILabel = {
        let temp = UILabel()
        temp.numberOfLines = 0
        temp.text = "今天注册了随心随记"
        temp.textColor = ColorAsset.c333333.value
        temp.font = FontAsset.PingFangSC_Medium.size(.Level14)
        return temp
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.backgroundColor = ColorAsset.ceeeeee.value
        self.backgroundColor = ColorAsset.ceeeeee.value
        
        self.contentView.addSubview(dayLab)
        self.contentView.addSubview(timeLab)
        self.contentView.addSubview(weekLab)
        self.contentView.addSubview(lineV)
        self.contentView.addSubview(imageV)
        self.contentView.addSubview(contentLab)
        self.selectionStyle = .none
        
        _cons()
    }
    
    func _cons() {
        dayLab.snp.makeConstraints { (make) in
            make.left.equalTo(15)
            make.top.equalTo(20)
        }
        
        weekLab.snp.makeConstraints { (make) in
            make.top.equalTo(23)
            make.left.equalTo(dayLab.snp.right).offset(3)
        }
        
        timeLab.snp.makeConstraints { (make) in
            make.top.equalTo(dayLab.snp.bottom).offset(10)
            make.left.equalTo(15)
        }
        
        lineV.snp.makeConstraints { (make) in
            make.left.equalTo(weekLab.snp.right).offset(2)
            make.top.bottom.equalTo(0)
        }
        
        imageV.snp.makeConstraints { (make) in
            make.top.equalTo(12)
            make.right.equalTo(-16)
            make.bottom.equalTo(0)
            make.left.equalTo(lineV.snp.right).offset(5)
        }
        
        contentLab.snp.makeConstraints { (make) in
            make.left.equalTo(imageV.snp.left).offset(20)
            make.top.equalTo(imageV.snp.top).offset(7)
            make.right.equalTo(imageV.snp.right).offset(-10)
            make.bottom.equalTo(imageV.snp.bottom).offset(-7)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
