//
//  UIButton+Timer.h
//  MeiMeiNew
//
//  Created by admin on 2019/5/24.
//  Copyright © 2019 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (Timer)

@property (nonatomic, copy) void (^startTiming)(UIButton * btn, NSInteger timeout);
@property (nonatomic, copy) void (^timing)(UIButton * btn, NSInteger timeout);
@property (nonatomic, copy) void (^endTiming)(UIButton * btn, NSInteger timeout);

@property (assign, nonatomic) NSInteger countTime;

-(void)fire;
@end

NS_ASSUME_NONNULL_END
