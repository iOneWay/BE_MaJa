//
//  MineViewController.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/28.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit


class MineVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    fileprivate var headBackIM = UIImageView(image: ImageAsset.mine_headback.value)
    fileprivate var nickLabel = UILabel()
    fileprivate var listTableView = UITableView(frame: CGRect.zero, style: .plain)
    fileprivate var array = [ImageAsset.mine_shoucang.value,ImageAsset.mine_tongzhi.value,ImageAsset.mine_xiugai.value,ImageAsset.mine_laji.value,ImageAsset.mine_shezhi.value] as [Any]
    fileprivate var arraytitle = ["我的收藏","系统通知","修改资料","垃圾箱","系统设置"]
    var qianming: UILabel?
    override func viewDidLoad() {
        super.viewDidLoad()

        initUI()
        self.navigationBar?.leftImg = UIImage()
        self.title = "我的"
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if UserInfo.default.signspecify.count > 0 {
                self.qianming?.text = UserInfo.default.signspecify
        }
        if UserInfo.default.nickname.count > 0 {
            
        }
        
        self.nickLabel.text = UserInfo.default.nickname
    }
    
    fileprivate func initUI(){
        
        self.view.addSubview(headBackIM)
        headBackIM.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(250)
        }
        let titleView = UIView()
        titleView.backgroundColor = .white
        self.view.addSubview(titleView)
        titleView.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(74)
            make.left.equalToSuperview().offset(13)
            make.right.equalToSuperview().offset(-13)
            make.height.equalTo(216)
        }
        
        let titleImage = UIImageView(image: ImageAsset.mine_titleImage.value)
        self.view.addSubview(titleImage)
        titleImage.snp.makeConstraints { (make) in
            make.centerY.equalTo(titleView.snp.top)
            make.width.height.equalTo(80)
            make.centerX.equalToSuperview()
        }
        
        nickLabel.text = "Steve Barnett";
        nickLabel.textColor = UIColor.black
        nickLabel.textAlignment = .center
        nickLabel.font = UIFont.systemFont(ofSize: 24)
        self.view .addSubview(nickLabel)
        
        nickLabel.snp.makeConstraints { (make) in
            make.top.equalTo(titleImage.snp.bottom).offset(24)
            make.centerX.equalToSuperview()
            make.height.equalTo(18)
            
        }
        
        
        let IDLabel = UILabel()
        IDLabel.textAlignment = .center
        IDLabel.textColor = .gray
        IDLabel.font = UIFont.systemFont(ofSize: 14)
        IDLabel.text = "ID:64894292"
        self.view.addSubview(IDLabel)
        IDLabel.snp.makeConstraints { (make) in
            make.top.equalTo(nickLabel.snp.bottom).offset(30)
            make.centerX.equalToSuperview()
            make.height.equalTo(15)
        }
        
        let singalLabel = UILabel()
        singalLabel.textAlignment = .left
        singalLabel.text = "个性签名"
        singalLabel.textColor = .black
        singalLabel.font = UIFont.systemFont(ofSize: 16)
        self.view.addSubview(singalLabel)
        singalLabel.snp.makeConstraints { (make) in
            make.left.equalTo(titleView.snp.left).offset(16)
            make.bottom.equalTo(titleView.snp.bottom).offset(-30)
            make.height.equalTo(23)
            make.width.equalTo(70)
        }
        
        self.qianming = UILabel()
        self.qianming?.text = "这个家伙很懒，什么都没写"
        self.qianming?.textColor = .gray
        self.qianming?.font = UIFont.systemFont(ofSize: 13)
        self.qianming?.textAlignment = .left
//        qianming.backgroundColor = .red
        self.view.addSubview(self.qianming!)
        self.qianming?.snp.makeConstraints { (make) in
            make.left.equalTo(singalLabel.snp.right).offset(15)
            make.centerY.equalTo(singalLabel.snp.centerY)
            make.right.equalTo(titleView.snp.right).offset(-16)
            make.height.equalTo(20)
        }
        
        self.listTableView.estimatedRowHeight = 100
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.showsVerticalScrollIndicator = false
        self.listTableView.separatorStyle = .none
        
        self.listTableView.tableFooterView = UIView()
        self.listTableView.backgroundColor = .clear
        
        self.listTableView.register(MinelistCell.self, forCellReuseIdentifier: MinelistCell.className)
        
        self.view.addSubview(self.listTableView)
        self.listTableView.snp.makeConstraints { (make) in
            make.top.equalTo(titleView.snp.bottom)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview().offset(0)
        }
        
        
        
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MinelistCell.className, for: indexPath) as? MinelistCell
        
    
        
        cell?.setBalanceData(array[indexPath.row + indexPath.section * 3] as! UIImage)

        cell?.setLeftTitle(arraytitle[indexPath.row + indexPath.section * 3] )
        return cell ?? MinelistCell();
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        return 2
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headView = UIView()
        headView.backgroundColor = ColorAsset.ceeeeee.value
        return headView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let type = indexPath.row + indexPath.section * 3
        switch type {
            
    
            
        case 0:
            let vc0 = ArticleListVC2()
            vc0.type = .collection
            
            self.navigationController?.pushViewController(vc0, animated: true)
            vc0.title = "我的收藏"
          break
        case 1:
            self.navigationController?.pushViewController(NotationVC(), animated: true)

            break
        case 2:
            self.navigationController?.pushViewController(DataVC(), animated: true)

            break
        case 3:
            let vc0 = ArticleListVC2()
            vc0.type = .dustbin
            
            self.navigationController?.pushViewController(vc0, animated: true)
            vc0.title = "垃圾箱"
            

            break
        case 4:
           
            self.navigationController?.pushViewController(settingVC(), animated: true)

            break
        default:
            break
        }
        
        
        
    }


    
    
}
