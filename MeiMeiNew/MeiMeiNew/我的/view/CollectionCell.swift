//
//  CollectionCell.swift
//  MeiMeiNew
//
//  Created by MAC on 2019/5/29.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class CollectionCell: UITableViewCell {

    fileprivate  let titleImage = UIImageView(image: ImageAsset.mine_titleImage.value)

    fileprivate  let nickName = UILabel()
    fileprivate  let TimeLabel = UILabel()
    fileprivate  let siagnalLabel = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        initUI()
     }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func initUI() {
        self.contentView.addSubview(titleImage)
        titleImage.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.top.equalTo(13)
            make.height.width.equalTo(44)
        }
        self.contentView.addSubview(nickName)
        nickName.text = "冰红茶"
        nickName.font = UIFont.systemFont(ofSize: 14)
        nickName.textAlignment = .left
        nickName.textColor = ColorAsset.c333333.value
        nickName.snp.makeConstraints { (make) in
            make.top.equalTo(titleImage)
            make.left.equalTo(titleImage.snp.right).offset(16)
            make.height.equalTo(20)
        }
        
        self.contentView.addSubview(TimeLabel)
        TimeLabel.textColor = ColorAsset.ceeeeee.value
        TimeLabel.font = UIFont.systemFont(ofSize: 12)
        TimeLabel.textColor = ColorAsset.c999999.value
        TimeLabel.text = "2019-11-12  12:30"
        TimeLabel.snp.makeConstraints { (make) in
            make.left.equalTo(nickName)
            make.height.equalTo(17)
            make.top.equalTo(nickName.snp
                .bottom).offset(6)
        }
    
    
        siagnalLabel.textColor = ColorAsset.c333333.value
        siagnalLabel.text = "而我很好 只缺了些烦恼也曾想过 若再遇到礼貌着说你的回忆就像电话偶尔打扰它说过去 是善意的玩笑如果各自安好…"
        siagnalLabel.font = UIFont.systemFont(ofSize: 12)
        siagnalLabel.numberOfLines = 2
        self.contentView.addSubview(siagnalLabel)
        siagnalLabel.snp.makeConstraints { (make) in
            make.left.equalTo(16)
            make.right.bottom.equalTo(-16)
            make.height.equalTo(35)
        }
    
        let lineLabel = UILabel()
        lineLabel.backgroundColor = ColorAsset.ceeeeee.value
        self.contentView.addSubview(lineLabel)
        lineLabel.snp.makeConstraints { (make) in
            make.height.equalTo(1)
            make.left.right.bottom.equalTo(0)
        }
    }
    
    
    
    func setImageData(_ balance: UIImage) {
        titleImage.image = balance
    }
    func setNameLabel(_ name:String) {
        nickName.text = name
    }
    func setSingalLabel(_ singal:String) {
        siagnalLabel.text  = singal
    }
    func setTimeLabel(_ time:String) {
        TimeLabel.text = time
    }
}
