//
//  LoginAPI.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/25.
//  Copyright © 2019 admin. All rights reserved.
//

import Foundation
import Moya
//首页

enum LoginAPI{
    case login(mobile: String, code: String)                    //登录
    case smsCode(mobile: String)                                //登录验证码
    case saveInfo(token: String, name: String)
}

extension LoginAPI{
    var path:String{
        switch self {
            case .login(_ , _):
                return "/api/user/login"
            case .smsCode(_):
                return "/api/user/send_sms"
        case .saveInfo(_, _):
                return "/api/user/saveInfo"
        }
    }
    
    var method: Moya.Method{
        return .get
    }
    
    var task:Task{
        switch self {
            case .login(let mobile, let code):
//                return .requestJSONEncodable( ["mobile":mobile, "code": code])
            return .requestParameters(parameters: ["mobile":mobile, "code": code], encoding: URLEncoding.default)
            case .smsCode(let mobile):
                return .requestParameters(parameters: ["mobile":mobile], encoding: URLEncoding.default)
            case .saveInfo(let token, let name):
                return .requestParameters(parameters: ["token":token, "name": name], encoding: URLEncoding.default)
        }
        
    }
}

