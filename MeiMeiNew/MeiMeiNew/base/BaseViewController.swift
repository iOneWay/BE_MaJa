//
//  BaseViewController.swift
//  newToken_swift
//
//  Created by 王伟 on 2018/12/25.
//  Copyright © 2018 王伟. All rights reserved.
//

import UIKit
import SnapKit

class BaseViewController: UIViewController {

    /// 是否支持滑动返回，默认支持
    public var canSlidBack = true {
        didSet {
            if canSlidBack {
                let target = self.navigationController?.interactivePopGestureRecognizer?.delegate;
                let pan = UIPanGestureRecognizer.init(target: target, action: nil)
                self.view.addGestureRecognizer(pan)
            }
        }
    }
    
    public var navigationBar: NavStyleProtocol?
    
    public var navStyle: NavStyle = .System
    {
        didSet {
            self.navigationBar = NavigationFactory.navigationBar(style: navStyle)
            self.view.addSubview(navigationBar as! UIView)
            self.navigationBar?.leftNavBtn?.addTarget(self, action: #selector(self.leftNavBtnClick(sender :)), for: .touchUpInside)
            self.navigationBar?.rightNavBtn?.addTarget(self, action: #selector(self.rightNavBtnClick(sender :)), for: .touchUpInside)
            self.navigationBar?.rightBtn?.addTarget(self, action: #selector(self.leftBtnClick(sender :)), for: .touchUpInside)
        }
    }
    
    override public var title: String? {
        didSet {
            self.navigationBar?.title = title ?? ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ColorAsset.ceeeeee.value
        self.navStyle = .System
        let view = self.navigationBar as? UIView
        view?.gradient()
//        view?.backgroundColor = ColorAsset.cfed103.value
    }
    
    @objc  public func leftNavBtnClick(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc  public func rightNavBtnClick(sender: UIButton) {}
    @objc  public func leftBtnClick(sender: UIButton) {}
    
    deinit {
        print("deinit\(String(describing: self.className))")
    }
}
