//
//  HDTabBarVC.swift
//  HDJ
//
//  Created by 王伟 on 2019/4/11.
//  Copyright © 2019 王伟. All rights reserved.
//

import UIKit

class HDTabBarVC: UITabBarController {

    private var _firtNV: UINavigationController?
    private var _secondNV: UINavigationController?
    private var _centerNV: UINavigationController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        _firtNV = generateNav(rootVC: DiaryListVC(), selectedImage:  ImageAsset.tab_jsb.value ?? UIImage() , normalImage:  ImageAsset.tab_jsb_sel.value ?? UIImage() )
        
        _secondNV = generateNav(rootVC: MineVC(),  selectedImage: ImageAsset.tab_person.value ?? UIImage(), normalImage:  ImageAsset.tab_person_sel.value ?? UIImage())
        
        _centerNV = generateNav(rootVC: CenterVC(),  selectedImage: ImageAsset.ic_center.value ?? UIImage(), normalImage:  ImageAsset.ic_center_sel.value ?? UIImage())
        
        self.viewControllers = [_firtNV, _centerNV,_secondNV] as? [UIViewController]
        UITabBar.appearance().backgroundColor = UIColor.white
        UITabBar.appearance().tintColor = ColorAsset.cB83AF3.value

//        let tabbar = HDTabBar()
//        self.setValue(tabbar, forKey: "tabBar")
//
//        tabbar.centerBtn.addTarget(self, action: #selector(centerBtnClick), for: .touchUpInside)
    }
    
    @objc func centerBtnClick(){
        /*
        let nav = UINavigationController(rootViewController: RegistVC())
        nav.navigationBar.isHidden = true
        self.present(nav , animated: true, completion: nil)
 */
    }
    private func generateNav(rootVC: UIViewController,
                             selectedImage: UIImage,
                             normalImage: UIImage) -> NavigationController
    {
        let nav = NavigationController(rootViewController: rootVC)
        let item = UITabBarItem()
        rootVC.title = ""
        item.image = normalImage
        item.selectedImage = selectedImage
        item.title = ""
//        item.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
//        item.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -7)
        nav.tabBarItem = item
        nav.navigationBar.isHidden = true
        return nav
    }

}
