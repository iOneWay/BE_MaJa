//
//  ViewController.swift
//  MeiMeiNew
//
//  Created by admin on 2019/5/23.
//  Copyright © 2019 admin. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationBar?.leftImg = ImageAsset.edit.value!
        self.navigationBar?.rightImg = ImageAsset.search.value!
    }
}
